<?php

/**
 * Rotas da API.
 * Todas as rotas são prefixadas com "api/v1"
 * e os controllers estão no namespace "App\Http\Controllers\Api"
 */

$router->get('dados-basicos', 'DadosBasicosController@index');

// Rotas de Estado
$router->get('estados', 'EstadosController@index');
$router->get('estados/{id}/municipios','EstadosController@getMunicipios');

//Rotas de município
$router->get('municipios/{id}/instituicoes','MunicipiosController@getInstituicoes');
$router->get('municipios/{id}/instituicoes/tipo/{tipo}','MunicipiosController@getInstituicoesByTipo');
$router->get('municipios/{id}/tipos-instituicoes','MunicipiosController@getTiposInstituicoes');
$router->get('municipios/{id}/bairros','MunicipiosController@getBairros');
$router->get('municipios/{id}/bairro/{bairro}/instituicoes','MunicipiosController@getInstituicoesBairro');
$router->get('municipios/{id}/bairro/{bairro}/tipos-instituicoes','MunicipiosController@getTiposInstituicoesBairro');

//Rotas de instituilçao
$router->get('instituicoes','InstituicoesController@index');
$router->get('instituicao/{id}/profissionais','InstituicoesController@getProfissionais');

//Respostas do formulário
$router->options('respostas', function () {
    return response('', 200);
});
$router->post('respostas','RespostasController@save');