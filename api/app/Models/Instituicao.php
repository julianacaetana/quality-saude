<?php
namespace App\Models;
  
use Illuminate\Database\Eloquent\Model;
  
class Instituicao extends BaseModel
{
    protected $table      = 'dfdwp.td_instituicao';
    protected $primaryKey = 'id_unidade';
    protected $keyType    = 'string';
    protected $guarded    = ['id_unidade'];

    public $timestamps = false;

    /**
     * Relação de município
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function municipio()
    {
        return $this->belongsTo(Municipio::class, 'id_municipio');
    }

    public function tiposInstituicao()
    {
        return $this->belongsTo(Municipio::class, 'id_municipio');
    }

    public function profissionais()
    {
        return $this->hasMany(Profissional::class, 'id_unidade', 'id_unidade');
    }
}