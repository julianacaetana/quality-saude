<?php
namespace App\Models;
  
use Illuminate\Database\Eloquent\Model;
  
class Municipio extends BaseModel
{
    protected $table      = 'dbgeral.tb_municipio';
    protected $primaryKey = 'id_municipio';
    protected $keyType    = 'string';
    protected $guarded   = ['id_municipio'];

    public $timestamps = false;

    /**
     * Relação de instituições
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function instituicoes()
    {
        return $this->hasMany(Instituicao::class, 'id_municipio', 'id_municipio');
    }

    public function getTiposInstituicoes()
    {
        $instituicaoTable = Instituicao::getTableName();
        $tipoInstituicaoTable = TipoInstituicao::getTableName();

        return \DB::table("$instituicaoTable as instituicao")
                    ->distinct()
                    ->select('tipo_unidade.id_tipo_unidade', 'tipo_unidade.ds_tipo_unidade')
                    ->join("$tipoInstituicaoTable as tipo_unidade", 'tipo_unidade.id_tipo_unidade', '=', 'instituicao.id_tipo_unidade')
                    ->where('instituicao.id_municipio', $this->{$this->primaryKey})
                    ->orderBy('tipo_unidade.ds_tipo_unidade')
                    ->get();
    }

    public function getTiposInstituicoesBairro($bairro)
    {
        $instituicaoTable = Instituicao::getTableName();
        $tipoInstituicaoTable = TipoInstituicao::getTableName();

        return \DB::table("$instituicaoTable as instituicao")
                    ->distinct()
                    ->select('tipo_unidade.id_tipo_unidade', 'tipo_unidade.ds_tipo_unidade')
                    ->join("$tipoInstituicaoTable as tipo_unidade", 'tipo_unidade.id_tipo_unidade', '=', 'instituicao.id_tipo_unidade')
                    ->where('instituicao.id_municipio', $this->{$this->primaryKey})
                    ->where('instituicao.no_bairro', $bairro)
                    ->orderBy('tipo_unidade.ds_tipo_unidade')
                    ->get();
    }
}