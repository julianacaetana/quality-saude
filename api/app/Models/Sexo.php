<?php
namespace App\Models;
  
use Illuminate\Database\Eloquent\Model;
  
class Sexo extends BaseModel
{
    protected $table      = 'dbgeral.td_sexo';
    protected $primaryKey = 'id_sexo';
    protected $fillable   = ['ds_sexo'];

    public $timestamps = false;
}