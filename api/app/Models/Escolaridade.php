<?php
namespace App\Models;
  
use Illuminate\Database\Eloquent\Model;
  
class Escolaridade extends BaseModel
{
    protected $table      = 'dfdwp.td_tipo_escolaridade';
    protected $primaryKey = 'id_tipo_escolaridade';
    protected $fillable   = ['ds_tipo_escolaridade'];

    public $timestamps = false;
}