<?php
namespace App\Models;
  
use Illuminate\Database\Eloquent\Model;
  
class Nota extends BaseModel
{
    protected $table = 'dfdwp.tf_notas';
    protected $primaryKey = 'id_nota';
    protected $guarded = ['id_nota'];

    public $timestamps = false;
}