<?php
namespace App\Models;
  
use Illuminate\Database\Eloquent\Model;
  
class NivelRenda extends BaseModel
{
    protected $table      = 'dfdwp.td_nivel_renda';
    protected $primaryKey = 'id';
    protected $fillable   = ['descricao'];

    public $timestamps = false;
}