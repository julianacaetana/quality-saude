<?php
namespace App\Models;
  
use Illuminate\Database\Eloquent\Model;
  
class Profissional extends BaseModel
{
    protected $table = 'dfdwp.td_profissional';
    protected $primaryKey = 'id_profissional';
    protected $keyType    = 'string';
    protected $guarded = ['id_profissional'];

    public $timestamps = false;

}