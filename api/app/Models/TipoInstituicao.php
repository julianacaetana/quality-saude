<?php
namespace App\Models;
  
use Illuminate\Database\Eloquent\Model;
  
class TipoInstituicao extends BaseModel
{
    protected $table      = 'dfdwp.td_tipo_unidade';
    protected $primaryKey = 'id_tipo_unidade';
    protected $fillable   = ['id_tipo_unidade'];

    public $timestamps = false;
}