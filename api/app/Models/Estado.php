<?php
namespace App\Models;
  
use Illuminate\Database\Eloquent\Model;
  
class Estado extends BaseModel
{
    protected $table      = 'dbgeral.tb_uf';
    protected $primaryKey = 'id_uf';
    protected $fillable   = ['uf_sigla', 'no_uf_completo'];

    public $timestamps = false;
}