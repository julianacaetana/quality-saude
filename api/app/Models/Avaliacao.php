<?php
namespace App\Models;
  
class Avaliacao extends BaseModel
{
    protected $table = 'dfdwp.td_avaliacoes';
    protected $primaryKey = 'id_avaliacao';
    protected $guarded = ['id_avaliacao'];

    public $timestamps = false;

    /**
     * Relação notas-profissionais
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notas()
    {
        return $this->hasMany(Nota::class, 'id_avaliacao', 'id_avaliacao');
    }

    /**
     * Relação notas-profissionais
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function profissionais()
    {
        return $this->belongsToMany(Profissional::class, 'dfdwp.rl_profissional_avaliacao', 'id_avaliacao', 'id_profissional');
    }
}