<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

class RespostasController extends Controller
{
    /**
     * @var Respostas
     */
    protected $respostasBusiness;

    /**
     * RespostasController constructor.
     * @param Respostas $respostasBusiness
     */
    public function __construct(\App\Business\Resposta $respostasBusiness)
    {
        $this->respostasBusiness = $respostasBusiness;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        $data = $request->all();
        try {
            $numProtocolo = $this->respostasBusiness->save($data);
            $response = ['error' => false, 'protocolo' => $numProtocolo];
        } catch (\Exception $e) {
            echo $e->getMessage();die;
            $response = ['error' => true, 'message' => 'Não foi possível salvar as respostas. Por favor, tente novamente.'];
        }

        return response()->json($response);
    }
}