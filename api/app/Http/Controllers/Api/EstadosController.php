<?php

namespace App\Http\Controllers\Api;

class EstadosController extends Controller
{
    protected $estadoBusiness;

    public function __construct(\App\Business\Estado $estadoBusiness)
    {
        $this->estadoBusiness = $estadoBusiness;
    }

    /**
     * End-point para busca de todos os estados
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $response = array_map(function($estado) {
            return [
                'id' => $estado['id_uf'],
                'nome' => $estado['no_uf_completo']
            ];
        }, $this->estadoBusiness->getAll()->toArray());

        return response()->json($response);
    }

    /**
     * End-point para de municípios de um estado
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMunicipios($id)
    {
        $response = array_map(function($estado) {
            return [
                'id' => $estado['id_municipio'],
                'nome' => $estado['no_mun_completo']
            ];
        }, $this->estadoBusiness->getMunicipios($id)->toArray());

        return response()->json($response);
    }
}