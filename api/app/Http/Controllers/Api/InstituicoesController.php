<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

class InstituicoesController extends Controller
{
    protected $instituicaoBusiness;

    /**
     * InstituicoesController constructor.
     * @param \App\Business\Instituicao $instituicaoBusiness
     */
    public function __construct(\App\Business\Instituicao $instituicaoBusiness)
    {
        $this->instituicaoBusiness = $instituicaoBusiness;
    }

    /**
     * End-point para busca de todas as instituições
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        // Apenas filtros permitidos
        $allowedParams = ['uf', 'municipio', 'bairro', 'tipo_instituicao'];
        $filters = array_filter($request->all(), function($param) use ($allowedParams) {
            return in_array($param, $allowedParams);
        }, ARRAY_FILTER_USE_KEY);

        $response = array_map(function($instituicao) {
            return [
                'id' => $instituicao['id_unidade'],
                'nome_fantasia' => $instituicao['no_fantasia']
            ];
        }, $this->instituicaoBusiness->getAll($filters)->toArray());

        return response()->json($response);
    }

    /**
     * End-point para de municípios de um estado
     * @param $id_instituicao
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProfissionais($id_instituicao)
    {
        $profissionais = $this->instituicaoBusiness->getProfissionais($id_instituicao)->toArray();

        $response = array_map(function($profissional) {
            return [
                'id' => $profissional['id_profissional'],
                'nome' => $profissional['no_profissional']
            ];
        }, $profissionais);

        return response()->json($response);
    }
}