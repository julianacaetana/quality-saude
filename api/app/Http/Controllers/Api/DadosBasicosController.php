<?php

namespace App\Http\Controllers\Api;

use Illuminate\Container\Container;

class DadosBasicosController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(\App\Business\DadosBasicos $dadosBasicosBusiness)
    {
        return response()->json($dadosBasicosBusiness->getAll());
    }
}