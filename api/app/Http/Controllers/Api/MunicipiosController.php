<?php

namespace App\Http\Controllers\Api;

class MunicipiosController extends Controller
{
    protected $municipioBusiness;

    public function __construct(\App\Business\Municipio $municipioBusiness)
    {
        $this->municipioBusiness = $municipioBusiness;
    }

    /**
     * End-point para busca de instituições de um município
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInstituicoes($municipio_id)
    {
        $response = array_map(function($instituicao) {
            return [
                'id'            => $instituicao['id_unidade'],
                'nome_fantasia' => $instituicao['no_fantasia']
            ];
        }, $this->municipioBusiness->getInstituicoes($municipio_id)->toArray());

        return response()->json($response);
    }

    /**
     * End-point para busca de instituições de um determinado de um município
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInstituicoesByTipo($municipio_id, $id_instituicao_tipo)
    {
        $response = array_map(function($instituicao) {
            return [
                'id'            => $instituicao['id_unidade'],
                'nome_fantasia' => $instituicao['no_fantasia']
            ];
        }, $this->municipioBusiness->getInstituicoesByTipo($municipio_id, $id_instituicao_tipo)->toArray());

        return response()->json($response);
    }

    /**
     * End-point para busca dos tipos de instituições de um município
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTiposInstituicoes($municipio_id)
    {
        $response = array_map(function($tipoInstituicao) {
            $instituicao = (array) $tipoInstituicao;
            return [
                'id'           => $instituicao['id_tipo_unidade'],
                'tipo_unidade' => $instituicao['ds_tipo_unidade']
            ];
        }, $this->municipioBusiness->getTiposInstituicoes($municipio_id)->toArray());

        return response()->json($response);
    }

    /**
     * End-point para busca dos bairros de um município
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBairros($municipio_id)
    {
        $response = array_map(function($bairro) {
            return [
                'nome' => $bairro['no_bairro']
            ];
        }, $this->municipioBusiness->getBairros($municipio_id)->toArray());

        return response()->json($response);
    }

    /**
     * End-point para busca dos bairros de um município
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInstituicoesBairro($bairro, $municipio_id)
    {
        $bairro = urldecode($bairro);
        $response = array_map(function($instituicao) {
            return [
                'id'            => $instituicao['id_unidade'],
                'nome_fantasia' => $instituicao['no_fantasia']
            ];
        }, $this->municipioBusiness->getInstituicoesBairro($municipio_id,  $bairro)->toArray());

        return response()->json($response);
    }

    /**
     * End-point para busca dos tipos de instituição de um bairro
     * @param $bairro
     * @param $municipio_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTiposInstituicoesBairro($bairro, $municipio_id)
    {
        $bairro = urldecode($bairro);
        $response = array_map(function($tipoInstituicao) {
            $instituicao = (array) $tipoInstituicao;
            return [
                'id'           => $instituicao['id_tipo_unidade'],
                'tipo_unidade' => $instituicao['ds_tipo_unidade']
            ];
        }, $this->municipioBusiness->getTiposInstituicoesBairro($municipio_id, $bairro)->toArray());

        return response()->json($response);
    }
}