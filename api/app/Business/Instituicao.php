<?php
namespace App\Business;

/**
 * Regras de negócio para "Instituição"
 */
class Instituicao {
    /**
     * @var \App\Models\Instituicao
     */
    protected $model;

    /**
     * Estado constructor.
     * @param \App\Models\Instituicao $instituicaoModel
     */
    public function __construct(\App\Models\Instituicao $instituicaoModel)
    {
        $this->model = $instituicaoModel;
    }

    /**
     * Busca todas as instituições cadastradas no sistema
     * @return \Illuminate\Database\Eloquent\Collection | \App\Models\Instituicao[]
     */
    public function getAll(array $filters)
    {
        $instituicaoTable = \App\Models\Instituicao::getTableName();
        $filterMap = [
            'bairro'           => 'no_bairro',
            'municipio'        => 'id_municipio',
            'tipo_instituicao' => 'id_tipo_unidade'
        ];
        $query = $this->model->select();

        foreach ($filters as $filter => $value) {
            if ($filter === 'uf') {
                $estadoTable = \App\Models\Estado::getTableName();
                $municipioTable = \App\Models\Municipio::getTableName();
                $query->join("$municipioTable as municipio", 'municipio.id_municipio', '=', "$instituicaoTable.id_municipio");
                $query->join("$estadoTable as uf", 'uf.id_uf', '=', 'municipio.id_uf');
                $query->where('uf.id_uf', $value);
            } else {
                $query->where("$instituicaoTable.{$filterMap[$filter]}", $value);
            }
        }

        return $query->get();
    }

    /**
     * @param string $id_unidade
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getProfissionais(string $id_unidade)
    {
        $instituicao = $this->model->find($id_unidade);
        $profissionais = $instituicao->profissionais()->distinct()->orderBy('no_profissional')->get();

        return $profissionais;
    }
}