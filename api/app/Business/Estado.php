<?php
namespace App\Business;

use App\Models\Municipio;

/**
 * Regras de negócio para "Estado"
 */
class Estado {
    /**
     * @var \App\Models\Estado
     */
    protected $model;

    /**
     * Estado constructor.
     * @param \App\Models\Estado $estadoModel
     */
    public function __construct(\App\Models\Estado $estadoModel)
    {
        $this->model = $estadoModel;
    }

    /**
     * Busca todos os estados cadastrados no sistema
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->model
                    ->whereNotIn('uf_sigla', ['IG', 'FN'])
                    ->orderBy('no_uf_completo')->get();
    }

    /**
     * Busca os municípios de um determinado estado
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getMunicipios($estado_id)
    {
        $municipioModel = new Municipio();
        return $municipioModel->where('id_uf', $estado_id)
                              ->orderBy('no_mun_completo')
                              ->get();
    }
}