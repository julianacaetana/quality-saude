<?php
namespace App\Business;

/**
 * Regras de negócio para "Município"
 */
class Municipio {
    /**
     * @var \App\Models\Municipio
     */
    protected $model;

    /**
     * Estado constructor.
     * @param \App\Models\Municipio $municipioModel
     */
    public function __construct(\App\Models\Municipio $municipioModel)
    {
        $this->model = $municipioModel;
    }

    /**
     * Busca todas as instituições de um município
     * @param $municipio_id
     * @return mixed
     */
    public function getInstituicoes($municipio_id)
    {
        return $this->model->find($municipio_id)
                           ->instituicoes()
                           ->get();
    }

    /**
     * Busca todas as instituições de um determinado tipo de um município
     * @param $municipio_id
     * @return mixed
     */
    public function getInstituicoesByTipo($municipio_id, $id_instituicao_tipo)
    {
        return $this->model->find($municipio_id)
                           ->instituicoes()->where('id_tipo_unidade', $id_instituicao_tipo)
                           ->get();
    }

    /**
     * Busca todas os tipos de instituições de um município
     * @param $municipio_id
     * @return mixed
     */
    public function getTiposInstituicoes($municipio_id)
    {
        return $this->model->find($municipio_id)
                           ->getTiposInstituicoes();
    }

    /**
     * Busca todas os bairros de um município
     * @param $municipio_id
     * @return \Illuminate\Database\Eloquent\Collection | \App\Models\Instituicao[]
     */
    public function getBairros($municipio_id)
    {
        return $this->model->find($municipio_id)
                           ->instituicoes()->select('no_bairro')->distinct()->orderBy('no_bairro')
                           ->get();
    }

    /**
     * Busca todas os bairros de um município
     * @param $municipio_id
     * @return \Illuminate\Database\Eloquent\Collection | \App\Models\Instituicao[]
     */
    public function getInstituicoesBairro($municipio_id, $bairro)
    {
        return $this->model->find($municipio_id)
                           ->instituicoes()->where('no_bairro', $bairro)
                           ->get();
    }

    /**
     * Busca todas os tipos de instituições de um bairro
     * @param $municipio_id
     * @return mixed
     */
    public function getTiposInstituicoesBairro($municipio_id, $bairro)
    {
        return $this->model->find($municipio_id)
                           ->getTiposInstituicoesBairro($bairro);
    }
}