<?php
namespace App\Business;

/**
 * Regras de negócio para "Dados Básicos"
 */
class DadosBasicos {
    /** @var \App\Models\Escolaridade */
    protected $escolaridadeModel;

    /** @var \App\Models\NivelRenda */
    protected $nivelRendaModel;

    /** @var \App\Models\Sexo */
    protected $sexoModel;

    /**
     * Escolaridade constructor.
     * @param \App\Models\Escolaridade $escolaridadeModel
     */
    public function __construct(
        \App\Models\Escolaridade $escolaridadeModel,
        \App\Models\NivelRenda $nivelRendaModel,
        \App\Models\Sexo $sexoModel
    ) {
        $this->escolaridadeModel = $escolaridadeModel;
        $this->nivelRendaModel = $nivelRendaModel;
        $this->sexoModel = $sexoModel;
    }

    /**
     * Busca todos as escolaridades cadastradas no sistema
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getAll() {
        return [
            'escolaridades' => $this->getEscolaridades(),
            'niveis_renda'  => $this->getNiveisRenda(),
            'sexos'         => $this->getSexos(),
        ];
        
    }

    public function getEscolaridades()
    {
        return $this->escolaridadeModel->orderBy('id_tipo_escolaridade')->get();
    }

    public function getNiveisRenda()
    {
        return $this->nivelRendaModel->orderBy('id')->get();
    }

    public function getSexos()
    {
        return $this->sexoModel->orderBy('id_sexo')->get();
    }
}