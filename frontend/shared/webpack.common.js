// Webpack uses this to work with directories
const path = require('path');

const isDevelopment = process.env.NODE_ENV === 'development';

// This is main configuration object.
// Here you write different options and tell Webpack what to do
module.exports = {

  // Path to your entry point. From this file Webpack will begin his work
  entry: './assets/js/index.js',

  // Path and filename of your result bundle.
  // Webpack will bundle all JavaScript into this file
  output: {
      filename: '[name].bundle.js'
  },

  optimization: {
    splitChunks: {
      chunks: 'all',
    },
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        include: [
          path.join(__dirname, 'assets/js')
        ],
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        options: {
          "plugins": ["@babel/plugin-transform-template-literals"]
        }
      },
      {
        // Now we apply rule for images
        test: /\.(png|jpe?g|gif|svg)$/,
        use: [
          {
            // Using file-loader for these files
            loader: "file-loader",

            // In options we can set different things like format
            // and directory to save
            options: {
              outputPath: 'img',
              esModule: false
            }
          }
        ]
      },
      {
        // Apply rule for fonts files
        test: /\.(woff|woff2|ttf|otf|eot)$/,
        use: [
          {
            // Using file-loader too
            loader: "file-loader",
            options: {
              outputPath: 'fonts'
            }
          }
        ]
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: { minimize: !isDevelopment }
          }
        ]
      }
    ]
  },

  resolve: {
    extensions: ['.js', '.jsx', '.scss']
  },

  devServer: {
    host: '0.0.0.0',
    disableHostCheck: true
  }
};