import {ajaxGet, ajaxPost} from '../../../shared/assets/js/api';

class AppBase {
    constructor() {
        this.$pages = $('#pt-main').find('.pt-page');
        this.current = 0;
        this.$currPage = this.$pages.eq(this.current);
        this.$nextPage = null;
        this.isAnimating = false;
        this.tipoDeAtendimento = null;
        this.isAnonimo = false;
        this.isRevisao = false;
        this.numPaginaInicialQuestoes = 4;
        this.numPaginaFinalQuestoes = 15;
        this.onAnimationEndHandler = () => { this.onAnimationEnd() };
        this.filtrosBuscaInstituicoes = {
            uf               : null,
            municipio        : null,
            bairo            : null,
            tipo_instituicao : null
        };

        const animEndEventNames = {
            'WebkitAnimation' : 'webkitAnimationEnd',
            'OAnimation' : 'oAnimationEnd',
            'msAnimation' : 'MSAnimationEnd',
            'animation' : 'animationend'
        };

        // animation end event name
        this.animEndEventName = animEndEventNames[ Modernizr.prefixed('animation') ];

        // Reseta os valores de todas as questões
        $('input[name^=qt]:checked').prop('checked', false);

        $('.nextPage').click(e => this.nextPage(e));
        $('.prevPage').click(e => this.prevPage(e));
        $('.btnRevisaoQts button').click(e => {
            this.isRevisao = true;
            $('.btn-acoes-wrapper > .btn-group').addClass('hidden');
            $('.btn-acoes-wrapper > button').removeClass('hidden');

            var numQuestao = $(e.target).data('qt');
            this.prevPage(e, this.getQtPageNumber(numQuestao));
        });
        this.$pages.each(function() {
            var $this = $(this);
            $this.data('originalClassList', $this.attr('class'));
        });

        this.$currPage.addClass('pt-page-current');

        this.registerListeners();
    }

    nextPage(event, next) {
        next = (next !== undefined ? next : this.current + 1);

        // Caso esteja em revisão de nota, volta para a tela de revisão de notas
        // e desmarca a flag de revisão
        if (this.isRevisao) {
            next = 17;
            this.isRevisao = false;
        }

        // Caso o usuário informe a satisfação, é obrigatória informar a expectativa e vice-versa
        if (this.current >= this.numPaginaInicialQuestoes && this.current <= this.numPaginaFinalQuestoes) {
            var numQt = this.getPageNumberQt(this.current).toString().padStart(2, 0),
                isExpectativaChecked = $('input[name=qt'+ numQt +'aExp]:checked').length !== 0,
                isSatisfacaoChecked = $('input[name=qt'+ numQt +'aSat]:checked').length !== 0;

            if (isSatisfacaoChecked && !isExpectativaChecked) {
                alert('Informe a sua expectativa!');
                return;
            }

            if (isExpectativaChecked && !isSatisfacaoChecked) {
                alert('Informe a sua satisfação!');
                return;
            }
        }

        var $evTarget = $(event.target);
        var animation = 5;

        // Validações específicas de algumas telas
        let nextPageMethod = `nextPage${next}`;
        if (typeof this[nextPageMethod] === 'function') {
            if (this[nextPageMethod]($evTarget) === false)
                return;
        }
        else
        switch(next) {
            case 1: // Dados básicos
                ajaxGet('dados-basicos', function(dados) {
                    // Adiciona as opções de "sexo"
                    var sexoSelect = $('#sexo'),
                        sexoHTML   = '';
                    
                    sexoSelect.empty();
                    $.each(dados.sexos, function(i, sexo) {
                        sexoHTML += '<option value="'+ sexo.id_sexo +'">'+ sexo.ds_sexo +'</option>';
                    });
                    sexoSelect.append(sexoHTML);

                    // Adiciona as opções de "escolaridade"
                    var escolaridadeSelect = $('#grau-escolaridade'),
                        escolaridadeHTML   = '';
                    
                    escolaridadeSelect.empty();
                    $.each(dados.escolaridades, function(i, escolaridade) {
                        escolaridadeHTML += '<option value="'+ escolaridade.id_tipo_escolaridade +'">'+ escolaridade.ds_tipo_escolaridade +'</option>';
                    });
                    escolaridadeSelect.append(escolaridadeHTML);

                    // Adiciona as opções de "nível de renda"
                    var nivelRendaSelect = $('#nivel-renda'),
                        nivelRendaHTML   = '';
                    
                    nivelRendaSelect.empty();
                    $.each(dados.niveis_renda, function(i, nivel_renda) {
                        nivelRendaHTML += '<option value="'+ nivel_renda.id +'">'+ nivel_renda.descricao +'</option>';
                    });
                    nivelRendaSelect.append(nivelRendaHTML);
                });
                break;

            case 4: //Seleção de profissional
                var tipoAtendimentoDOM = $('#selecao-tipo-atendimento').find('input[name=tipo-atendimento]:checked');
                if (tipoAtendimentoDOM.length === 0) {
                    alert("Selecione o tipo de atendimento!");
                    return;
                }
                this.tipoDeAtendimento = parseInt(tipoAtendimentoDOM.val());

                break;
            case 9: //Labels para as questões 06 e 07
                var labelQt06, labelQt07;
                switch (this.tipoDeAtendimento) {
                    case 1:
                        labelQt06 = 'MÉDICO(A)';
                        labelQt07 = 'do(a) MÉDICO(A)';
                        break;

                    case 2:
                        labelQt06 = 'ENFERMEIRO(A)';
                        labelQt07 = 'da ENFERMAGEM';
                        break;

                    case 3:
                        labelQt06 = 'DENTISTA';
                        labelQt07 = 'do(a) DENTISTA';
                        break;

                    case 4: // Setor de exames
                        labelQt06 = 'FARMACÊUTICO(A)';
                        labelQt07 = 'do(a) FARMACÊUTICO(A)';
                        break;

                    case 6: // Setor de farmácia avança para questão 08
                        this.nextPage(event, this.getQtPageNumber(8));
                        return;

                    case 5:
                        labelQt06 = 'FISIOTERAPEUTA';
                        labelQt07 = 'da FISIOTERAPIA';
                        break;

                    case 7:
                        labelQt06 = 'MÉDICO(A) PRESCRITOR(A)';
                        labelQt07 = 'do(a) MÉDICO(A) PRESCRITOR(A)';
                        break;

                    case 8:
                        labelQt06 = 'PSICÓLOGO(A)';
                        labelQt07 = 'da PSICOLOGIA';
                        break;

                    case 9:
                        labelQt06 = 'TERAPEUTA OCUPACIONAL';
                        labelQt07 = 'da TERAPIA OCUPACIONAL';
                        break;

                    case 10:
                        labelQt06 = 'ASSISTENTE SOCIAL';
                        labelQt07 = 'da ASSISTÊNCIA SOCIAL';
                        break;

                    case 11:
                        labelQt06 = 'NUTRICIONISTA';
                        labelQt07 = 'do(a) NUTRICIONISTA';
                        break;

                    case 12:
                        labelQt06 = 'FONOAUDIÓLOGO(A)';
                        labelQt07 = 'de FONOAUDIOLOGIA';
                        break;
                }
                $('#label-profissionalQt06').text(labelQt06);
                $('#label-profissionalQt07').text(labelQt07);
                break;

            case 11: //Labels para a questão 08
                var labelQt08;
                switch (this.tipoDeAtendimento) {
                    case 3: // Dentista
                    case 5: // e fisioterapia avança para questão 09
                        this.nextPage(event, this.getQtPageNumber(9));
                        return;

                    case 4:
                        labelQt08 = 'no SETOR DE EXAMES ou LABORATÓRIO';
                        break;

                    case 6:
                        labelQt08 = 'da FARMÁCIA';
                        break;

                    case 7:
                        labelQt08 = 'na ENFERMARIA de INTERNAMENTO';
                        break;

                    case 11: // Nutrição
                        labelQt08 = 'da NUTRIÇÃO';
                        break;

                    default: // Demais avançam para questão 10
                        this.nextPage(event, this.getQtPageNumber(10));
                        return;
                }
                $('#label-profissionalQt08').text(labelQt08);
                break;

            case 12: //Labels para a questão 09
                var labelQt09;
                $('#qt09Exames').hide();
                switch (this.tipoDeAtendimento) {
                    case 4: // Setor de exames ou laboratório
                        labelQt09 = 'a REALIZAÇÃO dos EXAMES';
                        $('#qt09Exames').show();
                        break;

                    case 6: // Setor de farmácia
                        labelQt09 = 'o RECEBIMENTO de MEDICAMENTOS';
                        break;

                    case 3: // Dentista
                    case 5: // Fisioterapia
                    case 7: // Atendimento durante internação
                        labelQt09 = 'o FUNCIONAMENTO dos EQUIPAMENTOS';
                        break;

                    case 11:
                        labelQt09 = 'o RECEBIMENTO de ALIMENTO/DIETA';
                        break;

                    default: // Demais avançam para questão 10
                        this.nextPage(event, this.getQtPageNumber(10));
                        return;
                }
                $('#label-profissionalQt09').text(labelQt09);
                break;

            case 17: // Tela de revisão de notas
                // Pega os valores das notas para revisão
                for (var qt = 1; qt <= 12; qt++) {
                    // Mostra apenas as questões do tipo de atendimento escolhido
                    if (qt === 6 || qt === 7) {
                        if ($.inArray(this.tipoDeAtendimento, [4, 6]) !== -1) {
                            $('.btnRevisaoQts div').eq(qt - 1).hide();
                            continue;
                        }
                        $('.btnRevisaoQts div').eq(qt - 1).show();
                    } else if (qt === 8) {
                        if ($.inArray(this.tipoDeAtendimento, [4, 6, 7]) === -1) {
                            $('.btnRevisaoQts div').eq(qt - 1).hide();
                            continue;
                        }
                        $('.btnRevisaoQts div').eq(qt - 1).show();
                    } else if (qt === 9) {
                        if ($.inArray(this.tipoDeAtendimento, [1, 2, 8, 9, 10]) !== -1) {
                            $('.btnRevisaoQts div').eq(qt - 1).hide();
                            continue;
                        }
                        $('.btnRevisaoQts div').eq(qt - 1).show();
                    }

                    var numQt = qt.toString().padStart(2, '0');
                    $('.notaExpRev').eq(qt - 1).text($('input[name=qt'+ numQt +'aExp]:checked').val());
                    $('.notaSatRev').eq(qt - 1).text($('input[name=qt'+ numQt +'aSat]:checked').val())
                }
                break;

            case 18: // Tela de finalização 1
                // Caso o usuário escolha não complementar a pesquisa,
                // avança para a tela de protocolo
                if ($evTarget.data('complementar') == 0) {
                    this.nextPage(event, 38);
                    return;
                }
                break;

            case 26:
                var labelQts09e10;
                switch (this.tipoDeAtendimento) {
                    case 1:
                        labelQts09e10 = 'dos MÉDICOS';
                        break;

                    case 2:
                        labelQts09e10 = 'dos ENFERMEIROS';
                        break;

                    case 3:
                        labelQts09e10 = 'dos DENTISTAS';
                        break;

                    case 4: // Setor de exames
                        labelQts09e10 = 'dos FARMACÊUTICOS';
                        break;

                    case 5:
                        labelQts09e10 = 'dos FISIOTERAPEUTAS';
                        break;

                    case 7:
                        labelQts09e10 = 'dos NUTRICIONISTAS';
                        break;
                }
                $('#label-profissionalQt09b, #label-profissionalQt10b').text(labelQts09e10);
                break;

            case 38: // Tela de finalização 2
                this.sendAnswers(function(data) {
                    if (!data) return;

                    $('#num_protocolo').text(data.protocolo);
                });
                break;

            case 39:
                this.resetForm();
                this.nextPage(event, 0);
                return;
        }

        this.animatePage(this.current, next, animation);
    }

    prevPage(event, prev) {
        prev = (prev !== undefined ? prev : this.current - 1);

        if (prev < 0) return;

        var animation = 6;

        switch (prev) {
            case 10:
                switch (this.tipoDeAtendimento) {
                    case 4: // Setor de exames
                    case 6: // Setor de farmácia voltam para questão 05
                        this.prevPage(event, this.getQtPageNumber(5));
                        return;
                }
                break;

            case 11:
                switch (this.tipoDeAtendimento) {
                    case 3: // Dentista
                    case 5: // Fisioterapia volta para questão 07
                        this.prevPage(event, this.getQtPageNumber(7));
                        return;
                }
                break;
            case 12:
                switch (this.tipoDeAtendimento) {
                    case 1: // Médico
                    case 2: // Enfermagem
                    case 8: // Psicologia
                    case 9: // Terapia ocupacional
                    case 10: // Assistência social
                    case 12: // Fonaudiologia volta para questão 07
                        this.prevPage(event, this.getQtPageNumber(7));
                        return;
                }
                break;
        }

        this.animatePage(this.current, prev, animation);
    }

    animatePage(currentPage, nextPage, animation) {
        if (this.isAnimating) return false;

        this.isAnimating = true;
        this.$currPage = this.$pages.eq(currentPage);
        this.$nextPage = this.$pages.eq(nextPage);

        // Volta a página para o topo, útil principalmente em smartphones
        this.$currPage.scrollTop(0);

        var outClass = '', inClass = '';
        switch (parseInt(animation)) {
            case 1:
                outClass = 'pt-page-moveToLeftEasing pt-page-ontop';
                inClass = 'pt-page-moveFromRight';
                break;
            case 2:
                outClass = 'pt-page-moveToRightEasing pt-page-ontop';
                inClass = 'pt-page-moveFromLeft';
                break;
            case 5:
                outClass = 'pt-page-fade';
                inClass = 'pt-page-moveFromRight pt-page-ontop';
                break;
            case 6:
                outClass = 'pt-page-fade';
                inClass = 'pt-page-moveFromLeft pt-page-ontop';
                break;
        }

        this.$currPage.addClass(outClass);

        this.$nextPage.on(this.animEndEventName, this.onAnimationEndHandler);
        this.$nextPage.addClass(inClass +' pt-page-current');

        this.current = nextPage;

        // Barra de progresso;
        var totalPages = this.current <= 18 ? 18 : 38,
            progress = (this.current * 100) / totalPages;
        $('#barra-progresso').width(progress+'%');
    }

    onAnimationEnd() {
        this.$nextPage.off(this.animEndEventName);
        this.$currPage.attr('class', this.$currPage.data('originalClassList'));
        this.$nextPage.attr('class', this.$nextPage.data('originalClassList') + ' pt-page-current');
        this.isAnimating = false;
    }

    /**
     * Retorna o número da página em que uma determinada questão se encontra
     * @param {*} qt 
     */
    getQtPageNumber(qt) {
        return this.numPaginaInicialQuestoes + (qt - 1);
    }

    /**
     * Retorna o número da questão que se encontra em determinada página
     * @param {*} page 
     */
    getPageNumberQt(page) {
        return page - (this.numPaginaInicialQuestoes - 1);
    }

    nextPage2($evTarget = {}) {
        if (
            !$('#ano-nascimento').val() ||
            !$('#sexo').val() || $('#sexo').val() == '-1' ||
            !$('#grau-escolaridade').val() || $('#grau-escolaridade').val() == '-1' ||
            !$('#nivel-renda').val() || $('#nivel-renda').val() == '-1' ||
            !$('#possui-plano').val()
        ) {
            alert('Informe os dados obrigatórios!');
            return false;
        }

        this.isAnonimo = ($evTarget.data('anonimo') === 1);

        ajaxGet('estados', function(estados) {
            // Adiciona as opções de "UF"
            var ufSelect = $('#uf'),
                ufHTML   = '';
                
            ufSelect.empty();
            $.each(estados, function(i, uf) {
                ufHTML += '<option value="'+ uf.id +'">'+ uf.nome +'</option>';
            });
            ufSelect.append(ufHTML);
            ufSelect.change();
        });
    }

    registerListeners() {
        $(() => {
            this.resetForm();

            // Máscaras
            $('#cpf-rg').mask('00000000000000000000');
            $('#ano-nascimento').mask('0000');

            let maskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            };
        
            $('#tel-1, #tel-2').mask(maskBehavior, {
                onKeyPress: function(val, e, field, options) {
                    field.mask(maskBehavior.apply({}, arguments), options);
                }
            });

            $('#ano-nascimento').change(e => {
                let ano    = e.target.value,
                    anoMax = (new Date()).getFullYear(),
                    anoMin = anoMax - 120;

                if (ano < anoMin || ano > anoMax) {
                    alert('Ano informado é inválido!');
                    e.target.value = '';
                }
            });
        });

        // Altera a classe dos inputs de "Tipo de atendimento"
        $('input[name=tipo-atendimento]').click(e => {
            $('#selecao-tipo-atendimento').find('label.active').removeClass('active');
            $(e.target).parent().addClass('active');
            $('#profissionais').prop('multiple', (e.target.value == 7));
        
            //@TODO: Verificar scroll to bottom
            // transitions.$nextPage.scrollTop = transitions.$nextPage.scrollHeight;
        });

        // Altera a classe dos inputs dos níveis de satisfação
        $('.niveis-satisfacao').click(e => {
            var $this = $(e.target);

            if (!$this.hasClass('niveis-satisfacao')) { return; }

            $this.closest('div.row').find('.radio-checked').removeClass('radio-checked');
            $this.addClass('radio-checked');
            $this.find('input').prop('checked', true);
        });

        this.registerUFAndMunicipioListener();

        $('#bairro').change(e => {
            // Ao alterar o bairro, é necessário remover o filtro de menor hierarquia
            this.filtrosBuscaInstituicoes.tipo_instituicao = null;
        
            let selectedMunicipio = this.filtrosBuscaInstituicoes.municipio,
                selectedBairro = $(e.target).val();
            if (!selectedBairro) {
                this.filtrosBuscaInstituicoes.bairro = null;
                this.getMunicipioTiposInstituicoes(selectedMunicipio);
            } else {
                this.filtrosBuscaInstituicoes.bairro = selectedBairro;
        
                ajaxGet(`municipios/${selectedMunicipio}/bairro/${selectedBairro}/tipos-instituicoes`, function(tiposUnidades) {
                    // Adiciona as opções de "Tipo da instituição"
                    let tiposUnidadesSelect = $('#tipo-instituicao'),
                        tiposUnidadesHTML   = '<option value="">Selecione o tipo de instiuição</option>';
            
                    tiposUnidadesSelect.empty();
                    $.each(tiposUnidades, function(i, tipoUnidade) {
                        tiposUnidadesHTML += '<option value="'+ tipoUnidade.id +'">'+ tipoUnidade.tipo_unidade +'</option>';
                    });
                    tiposUnidadesSelect.append(tiposUnidadesHTML);
                });
            }
        
            this.buscaInstituicoes();
        });

        $('#tipo-instituicao').change(e => {
            let selectedTipo = $(e.target).val();

            this.filtrosBuscaInstituicoes.tipo_instituicao = selectedTipo || null;
            this.buscaInstituicoes();
        });
        
        $('#instituicao').change(function() {
            let selectedInstituicao = $(this).val(),
                profissionalSelect  = $('#profissionais-group');
        
            if (!selectedInstituicao) {
                profissionalSelect.empty();
                profissionalSelect.append($('<option>', {value: null, text: 'Sem profissional cadastrado para a unidade selecionada.'}));
                return;
            }
        
            ajaxGet(`instituicao/${selectedInstituicao}/profissionais`, profissionais => {
                profissionalSelect.empty();

                if (profissionais.length === 0) {
                    profissionalSelect.append($('<option>', {value: null, text: 'Sem profissional cadastrado para a unidade selecionada.'}));
                    return;
                }

                // Adiciona as opções de "Município"
                let profissionalHTML = '';
                $.each(profissionais, function(i, profissional) {
                    profissionalHTML += '<option value="'+ profissional.id +'">'+ profissional.nome +'</option>';
                });
                profissionalSelect.append(profissionalHTML);
            });
        });
    }

    registerUFAndMunicipioListener() {
        $('#uf').change(e => {
            // Ao alterar a uf, é necessário remover os filtros de menor hierarquia
            this.filtrosBuscaInstituicoes.municipio = null;
            this.filtrosBuscaInstituicoes.bairo = null;
            this.filtrosBuscaInstituicoes.tipo_instituicao = null;

            let selectedUf = $(e.target).val();
            if (!selectedUf) return;

            this.filtrosBuscaInstituicoes.uf = selectedUf;

            ajaxGet(`estados/${selectedUf}/municipios`, function(municipios) {
                // Adiciona as opções de "Município"
                let municipioSelect = $('#municipio'),
                    municipiosHTML  = '';
        
                municipioSelect.find('option:not(:first)').remove();
                $.each(municipios, function(i, municipio) {
                    municipiosHTML += '<option value="'+ municipio.id +'">'+ municipio.nome +'</option>';
                });
                municipioSelect.append(municipiosHTML);
            });
        
            this.buscaInstituicoes();
        });

        $('#municipio').change(e => {
            // Ao alterar o munícipio, é necessário remover os filtros de menor hierarquia
            this.filtrosBuscaInstituicoes.bairo = null;
            this.filtrosBuscaInstituicoes.tipo_instituicao = null;

            var selectedMunicipio = $(e.target).val();
            if (!selectedMunicipio) {
                this.filtrosBuscaInstituicoes.municipio = null;
                return;
            } else {
                this.filtrosBuscaInstituicoes.municipio = selectedMunicipio;
            }
        
            this.getMunicipioBairros(selectedMunicipio);
            this.getMunicipioTiposInstituicoes(selectedMunicipio);
            this.buscaInstituicoes();
        });
    }

    /**
     * Reseta os campos do formulário
     */
    resetForm() {
        $('form')[0].reset();
        $('#selecao-tipo-atendimento .active').removeClass('active');
        $('label.radio-checked').removeClass('radio-checked');
        $('.notaExpRev, .notaSatRev, #num_protocolo').text('');
        this.isAnonimo = false;
    }

    buscaInstituicoes() {
        // Remove todos os valores "null" para não enviar na requisição
        let notNullFilters = $.extend({}, this.filtrosBuscaInstituicoes);
        $.each(notNullFilters, (i, filter) => {
            if (!filter) delete notNullFilters[i];
        });

        ajaxGet('instituicoes?'+ $.param(notNullFilters), function(instituicoes) {
            // Adiciona as opções de "Município"
            var instituicaoSelect = $('#instituicao'),
                instituicaoHTML   = '';
    
            instituicaoSelect.empty();
            $.each(instituicoes, function(i, instituicao) {
                instituicaoHTML += '<option value="'+ instituicao.id +'">'+ instituicao.nome_fantasia +'</option>';
            });
            instituicaoSelect.append(instituicaoHTML);
            instituicaoSelect.change();
        });
    }

    getMunicipioBairros(municipioId) {
        ajaxGet('municipios/'+ municipioId +'/bairros', function(bairros) {
            // Adiciona as opções de "Bairro"
            var bairrosSelect = $('#bairro'),
                bairrosHTML   = '<option value="">Selecione o bairro</option>';
    
            bairrosSelect.empty();
            $.each(bairros, function(i, bairro) {
                bairrosHTML += '<option value="'+ bairro.nome +'">'+ bairro.nome +'</option>';
            });
            bairrosSelect.append(bairrosHTML);
        });
    }

    getMunicipioTiposInstituicoes(municipioId) {
        ajaxGet('municipios/'+ municipioId +'/tipos-instituicoes', function(tiposUnidades) {
            // Adiciona as opções de "Tipo da instituição"
            var tiposUnidadesSelect = $('#tipo-instituicao'),
                tiposUnidadesHTML   = '<option value="">Selecione o tipo de instiuição</option>';
    
            tiposUnidadesSelect.empty();
            $.each(tiposUnidades, function(i, tipoUnidade) {
                tiposUnidadesHTML += '<option value="'+ tipoUnidade.id +'">'+ tipoUnidade.tipo_unidade +'</option>';
            });
            tiposUnidadesSelect.append(tiposUnidadesHTML);
        });
    }

    sendAnswers(callback) {
        var sData = this.serializeAnswers();
        ajaxPost('respostas', sData, function(data) {
            if (!data) throw new Error('Aborted');

            if (data.error === true) throw new Error(data.message);

            callback(data);
        });
    }

    serializeAnswers() {
        var inputs = $('input[type=text], input[type=email], input[type=radio]:checked, select, textarea'),
            serializedData = $.extend({}, this.getAditionalSerializeData());

        inputs.each(function (i, input) {
            serializedData[input.name || input.id] = $(input).val();
        });

        if (this.isAnonimo) serializedData.isAnonimo = true;

        return serializedData;
    }

    getAditionalSerializeData() {
        return {}
    }
};

export { AppBase };