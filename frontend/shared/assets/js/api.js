/**
 * Parâmatros de configuração base para AJAX
 * @type {string}
 */
const baseURL = 'https://api.ideatecs.local/v1/';
const ajaxDefaultParams = {
    headers: {'Accept': 'application/json'}
};

/**
 * Realiza uma requisão GET para a URL informada
 * @param url
 * @param callback
 */
function ajaxGet(url, callback) {
    var options = $.extend({
        url     : baseURL + url,
        success : callback
    }, ajaxDefaultParams);
    $.get(options);
};

/**
 * Realiza uma requisão GET para a URL informada
 * @param url
 * @param callback
 */
function ajaxPost(url, data, callback) {
    var options = $.extend({
        url     : baseURL + url,
        data    : data,
        success : callback
    }, ajaxDefaultParams);
    $.post(options);
};

export {ajaxGet, ajaxPost};