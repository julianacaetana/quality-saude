// It is handy to not have those transformations while we developing
module.exports = process.env.NODE_ENV === 'production' ? {
    plugins: [
        require('autoprefixer'),
        require('cssnano'),
        // More postCSS modules here if needed
    ]
} : {};
