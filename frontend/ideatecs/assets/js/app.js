import { AppBase } from '../../../shared/assets/js/app-base';

class App extends AppBase {
    constructor() {
        super();
    }
};

export { App };