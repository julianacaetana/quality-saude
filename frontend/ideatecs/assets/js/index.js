// import crc16 from 'crc';

// Styles
import '../scss/bootstrap-custom.scss';
import '../../../shared/assets/css/animations.css';
import '../../../shared/assets/css/style.css';
import '../../../shared/assets/css/responsive.css';
import '../../../shared/assets/css/fonts.css';

// Service Worker Registration
import 'script-loader!../../service-worker-registration';

// Scripts Vendor
import 'script-loader!jquery';
import 'script-loader!jquery-mask-plugin';

// Scripts App
import 'script-loader!../../../shared/assets/js/modernizr-custom';
import { App } from './app';

const instance = new App();