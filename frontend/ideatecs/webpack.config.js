const merge = require('webpack-merge');
const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const {GenerateSW} = require('workbox-webpack-plugin');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const webpackCommonConfig = require('../shared/webpack.common');

module.exports = merge(webpackCommonConfig, {
  // Path and filename of your result bundle.
  // Webpack will bundle all JavaScript into this file
  output: {
    path: path.resolve(__dirname, 'dist')
  },

  module: {
    rules : [
      {
        test: /\.(sa|sc|c)ss$/,
        exclude: /\.module.((sa|sc|c)ss)$/,
        loader: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'postcss-loader',
          'sass-loader'
        ]
      }
    ]
  },

  plugins: [
    new HtmlWebPackPlugin({
      template: './index.html',
      filename: './index.html'
    }),

    new MiniCssExtractPlugin({
      filename: "bundle.css"
    }),

    new CopyPlugin([
      { from: `../shared/assets/js/polyfills.js`, to: './' },
      { from: `../shared/assets/img/icons`, to: './img/icons' }
    ]),

    new GenerateSW({
      'exclude' : ['.htaccess']
    })
  ],

  devServer : {
    public: 'ideatecs.local'
  }
});
