import { AppBase } from '../../../shared/assets/js/app-base';

class App extends AppBase {
    constructor() {
        super();
    }

    nextPage2($evTarget = {}) {
        if (
            !$('#ano-nascimento').val() ||
            !$('#sexo').val() || $('#sexo').val() == '-1' ||
            !$('#grau-escolaridade').val() || $('#grau-escolaridade').val() == '-1' ||
            !$('#nivel-renda').val() || $('#nivel-renda').val() == '-1' ||
            !$('#possui-plano').val()
        ) {
            alert('Informe os dados obrigatórios!');
            return false;
        }

        this.isAnonimo = ($evTarget.data('anonimo') === 1);

        let municipioId = 230428; // Eusébio-CE
        this.filtrosBuscaInstituicoes.municipio = municipioId;
        this.getMunicipioBairros(municipioId);
        this.getMunicipioTiposInstituicoes(municipioId);
        this.buscaInstituicoes();
    }

    // Eusébio não precisa selecionar UF nem Município
    registerUFAndMunicipioListener() {}

    getAditionalSerializeData() {
        return {
            fase: 11 // Fase implantação em EUSÉBIO-CE
        }
    }
}

export { App };